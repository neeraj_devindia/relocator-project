﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    [MetadataType(typeof(PersonValidation))]
    public partial class Person
    {
       
    }

    public class PersonValidation
    {
        public object Id { get; set; }
        [Required]
        public object Name { get; set; }
        [Required]
        public object Email { get; set; }
        [Required]
        public string Password { get; set; }
       
    }
}
