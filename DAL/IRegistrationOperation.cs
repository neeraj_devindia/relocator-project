﻿using System.Collections.Generic;
using System.Linq;

namespace DAL
{
    public interface IRegistrationOperation
    {
        /// <summary>
        /// Get all Motivation records from Motivation
        /// </summary>
        /// <returns></returns>
        IQueryable<Motivation> GetMotivations();

        /// <summary>
        /// Get Motivation Table by Id. Return type is object, because records will return from many tables.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        object GetMotivationSelectedTableList(int id);

        /// <summary>
        /// Load WorkSector table.
        /// </summary>
        /// <returns></returns>
        IQueryable<WorkSector> LoaWorkSectors();

        /// <summary>
        /// Load ITBranch table.
        /// </summary>
        /// <returns></returns>
        IQueryable<ITBranch> LoadItBranches();

        /// <summary>
        /// Load MedicalSector table
        /// </summary>
        /// <returns></returns>
        IQueryable<MedicalSector> LoadMedicalSectors();

        IQueryable<Person> LoadPeople();

        Person LoadPersonal(string email);

        Person LoadPerson(long id);

        void SavePerson(Person person);
        void EditPerson(Person person);

        System.Collections.IEnumerable LoadEntrepeneurs();

        System.Collections.IEnumerable LoadStudies();

        System.Collections.IEnumerable LoadFamilyStatus();

        System.Collections.IEnumerable LoadTourism();

        System.Collections.IEnumerable LoadWork();

        System.Collections.IEnumerable LoadGerman();

        System.Collections.IEnumerable LoadFamily();

        System.Collections.IEnumerable LoadStudyLevel();

        System.Collections.IEnumerable LoadUniversityTypes();

        System.Collections.IEnumerable LoadFieldsOfStudies();

        System.Collections.IEnumerable LoadFamilyRelations();

        System.Collections.IEnumerable LoadSpouses();

        void DeletePerson(string email);


    }
}
