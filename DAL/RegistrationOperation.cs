﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DAL
{
    public class RegistrationOperation : IRegistrationOperation
    {
        private readonly ReclocatorDBEntities _relocatorDbEntities;

        public RegistrationOperation()
        {
            _relocatorDbEntities = new ReclocatorDBEntities();
            _relocatorDbEntities.Configuration.LazyLoadingEnabled = true;
        }

        public IQueryable<Motivation> GetMotivations()
        {
            var motivations = from m in _relocatorDbEntities.Motivations select m;
            return motivations;
        }

        public object GetMotivationSelectedTableList(int id)
        {
            var getRecords = GetMotimationRelatedRecords(id);
            if (getRecords == null) return null;
            return getRecords;
        }

        private object GetMotimationRelatedRecords(int id)
        {
            object getRecords;
            switch (id)
            {
                case 1:
                    getRecords = from m in _relocatorDbEntities.Works select m;
                    break;
                case 2:
                    getRecords = from m in _relocatorDbEntities.Entrepeneurs select m;
                    break;
                case 3:
                    getRecords = from m in _relocatorDbEntities.Studies select m;
                    break;
                case 4:
                    getRecords = from m in _relocatorDbEntities.Families select m;
                    break;
                case 5:
                    getRecords = from m in _relocatorDbEntities.Tourism select m;
                    break;
                default:
                    return null;
            }
            return getRecords;
        }

        public IQueryable<WorkSector> LoaWorkSectors()
        {
            var records = from v in _relocatorDbEntities.WorkSectors select v;
            return records;
        }

        public IQueryable<ITBranch> LoadItBranches()
        {
            var records = from v in _relocatorDbEntities.ITBranches select v;
            return records;
        }

        public IQueryable<MedicalSector> LoadMedicalSectors()
        {
            var recods = from v in _relocatorDbEntities.MedicalSectors select v;
            return recods;
        }

        public IQueryable<Person> LoadPeople()
        {
            return _relocatorDbEntities.People;
        }

        public string GetName(int id)
        {
            return _relocatorDbEntities.People.Where(x => x.Id == id).Select(y => y.Name).First();
        }

        public Person LoadPerson(long id)
        {
            var test = _relocatorDbEntities.People.FirstOrDefault(p => p.Id == id);
            return test;
        }
        public Person LoadPersonal(string email)
        {
            return _relocatorDbEntities.People.FirstOrDefault(p => p.Email == email);
        }

        public void SavePerson(Person person)
        {
            _relocatorDbEntities.People.Add(person);
            _relocatorDbEntities.SaveChanges();
        }


        public void EditPerson(Person person)
        {
            //_relocatorDbEntities.Entry(person.Work).State = EntityState.Detached;
            var test = _relocatorDbEntities.People.SingleOrDefault(p => p.Id == person.Id);
            CopyPropertyValues(person, test);
            //test.Motivation_Id = person.Motivation_Id;
            test.Work = person.Work;
            test.Tourism = person.Tourism;
            test.Entrepeneur = person.Entrepeneur;
            test.Study = person.Study;
            test.Family = person.Family;
            CopyPropertyValues(test.Work, person.Work);
            CopyPropertyValues(test.Study, person.Study);
            CopyPropertyValues(test.Entrepeneur, person.Entrepeneur);
            CopyPropertyValues(test.Family, person.Family);
            CopyPropertyValues(test.Tourism, person.Tourism);
            _relocatorDbEntities.SaveChanges();
        }
        public static void CopyPropertyValues(object source, object destination)
        {
            if (source == null) return;
            if (destination == null) return;
            var destProperties = destination.GetType().GetProperties();

            foreach (var sourceProperty in source.GetType().GetProperties())
            {
                foreach (var destProperty in destProperties)
                {
                    if (destProperty.Name == sourceProperty.Name &&
                destProperty.PropertyType.IsAssignableFrom(sourceProperty.PropertyType))
                    {
                        destProperty.SetValue(destination, sourceProperty.GetValue(
                            source, new object[] { }), new object[] { });

                        break;
                    }
                }
            }
        }
        public IEnumerable LoadEntrepeneurs()
        {
            return _relocatorDbEntities.Entrepeneurs;
        }

        public IEnumerable LoadStudies()
        {
            return _relocatorDbEntities.Studies;
        }

        public IEnumerable LoadFamilyStatus()
        {
            return _relocatorDbEntities.FamilyStatus;
        }

        public IEnumerable LoadTourism()
        {
            return _relocatorDbEntities.Tourism;
        }

        public IEnumerable LoadWork()
        {
            return _relocatorDbEntities.Works;
        }

        public IEnumerable LoadGerman()
        {
            return _relocatorDbEntities.LevelOfGermen;
        }

        public IEnumerable LoadFamily()
        {
            return _relocatorDbEntities.Families;
        }

        public IEnumerable LoadStudyLevel()
        {
            return _relocatorDbEntities.StudyLevels;
        }

        public IEnumerable LoadUniversityTypes()
        {
            return _relocatorDbEntities.UniversityTypes;
        }

        public IEnumerable LoadFieldsOfStudies()
        {
            return _relocatorDbEntities.FieldOfStudies;
        }

        public IEnumerable LoadFamilyRelations()
        {
            return _relocatorDbEntities.FamilyRelations;
        }

        public IEnumerable LoadSpouses()
        {
            return _relocatorDbEntities.Spouses;
        }

        public void DeletePerson(string email)
        {
            var person = (from v in _relocatorDbEntities.People where v.Email == email select v).FirstOrDefault();

            switch (person.Motivation_Id)
            {
                case 1:
                    {
                        _relocatorDbEntities.Entry(person.Work).State = EntityState.Deleted;
                        break;
                    }
                case 2:
                    {
                        _relocatorDbEntities.Entry(person.Entrepeneur).State = EntityState.Deleted;
                        break;
                    }
                case 3:
                    {
                        _relocatorDbEntities.Entry(person.Study).State = EntityState.Deleted;
                        break;
                    }
                case 4:
                    {
                        _relocatorDbEntities.Entry(person.Family).State = EntityState.Deleted;
                        break;
                    }
                case 5:
                    {
                        _relocatorDbEntities.Entry(person.Tourism).State = EntityState.Deleted;
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            _relocatorDbEntities.People.Remove(person);
            _relocatorDbEntities.SaveChanges();
        }
    }
}