﻿using System.Web.Mvc;

namespace Relocator_Project.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

    }
}
