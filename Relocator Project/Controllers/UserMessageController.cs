﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DAL;
using WebMatrix.WebData;

namespace Relocator_Project.Controllers
{
    [AllowAnonymous]
    public class UserMessageController : Controller
    {
        public static string EvaluateStatus(Person data, Work work, Family family, Tourism tour, Study study,
                                            Entrepeneur ent, string nonsense)
        {
            string message = string.Empty;

            if (data.Motivation_Id == 6)
            {
                message = GetMotivationMessage();
            }
            if (data.Motivation_Id == 5)
            {
                message = GetTourismMessage(data, tour);
            }
            if (data.Motivation_Id == 4)
            {
                message = GetFamilyMessage(data, family);
            }
            if (data.Motivation_Id == 3)
            {
                message = GetStudyMessage(data, study);
            }
            if (data.Motivation_Id == 2)
            {
                message = GetEntrepeneurMessage(data, ent);
            }
            if (data.Motivation_Id == 1)
            {
                message = GetWorkMessage(data, work);
            }
            return message;
        }

        private static string GetWorkMessage(Person data, Work work)
        {
            string message = string.Empty;
            if (work.WorkSector_Id == 1 && work.MedicalSector_Id == 1)
            {
                message = @"EVAL: you have high chances to find a job in D. ! + evaluation of 1.1,1.2,1.3 . If you want to have a more detailed overview of things to consider in your case please book our service for 50 euros and receive discount on further services you book with us.";
                if (data.LevelOfGerman_Id == 1 || data.LevelOfGerman_Id == 2 || data.LevelOfGerman_Id == 4)
                {
                    message = @"To work in Germany as a doctor  you have to provide at least a B2 level
The only acknowledged official certificates:
telc and Goethe
You also have to pass a medical exam held by the German medical association
Requirements:
- highly advanced skills in medical German (C1)
We offer a special German course preparing doctors for this exam (online or in Germany) ";
                }

                if (data.LevelOfGerman_Id >= 5)
                {
                    message = @"Has your medical degree already been recognized in Germany?";
                }
            }

            if (work.WorkSector_Id == 1 && work.MedicalSector_Id == 2)
            {
                message = @"RESEARCH NEEDED you might have a chance, please press this button for more information";
            }
            if (work.WorkSector_Id == 1 && work.MedicalSector_Id == 3)
            {
                message = @"you might have a chance to find work in Germany. Please press this button for more information";
            }
            //Others--------------------------------
            if (work.WorkSector_Id == 3)
            {
                message =
                    @"You might have chances to come to Germany, if you want to know more about your specific case please book our service here";
                if (work.DoYouHaveAcademicDegree.HasValue && work.DoYouHaveAcademicDegree.GetValueOrDefault())
                {
                    message =
                    @"please note that you have to pay  50 euros to get an individual evaluation for your chances";

                    if (work.DoYouHaveWorkContract.HasValue && work.DoYouHaveWorkContract == true)
                    {
                        message = @"Is salary above 3,967 €/month (47,600 €/year)?";
                    }
                    if (work.DoYouHaveWorkContract.HasValue && work.DoYouHaveWorkContract == false)
                    {
                        message = @"What is your degree and from which university is it?";
                    }
                }
                if (work.DoYouHaveAcademicDegree.HasValue && work.DoYouHaveAcademicDegree == false)
                {
                    message = @"qualifizierte Fachkräfte? CHECKqualifying/  professional formation (?) with at least 2 years";
                }
            }
            //IT-------------------------------
            if (work.WorkSector_Id == 2 && work.DoYouHaveAcademicDegree.HasValue && work.DoYouHaveAcademicDegree.GetValueOrDefault())
            {
                if (work.ITBranch_Id == 1 || work.ITBranch_Id == 2 || work.ITBranch_Id == 3)
                {
                    message = @"You have very good chances to come to Germany. Germany is looking for IT people
+ evaluation of 1.1,1.2,1.3 ., if you want to know more about your specific case please book our service here";

                    if (work.DoYouHaveWorkContract.HasValue && work.DoYouHaveWorkContract == true)
                    {
                        if (work.SalaryAmount > 3967)
                        {
                            message = @"FINAL EVAL: if a and yes: You have the minimum salary required for a Blue Card which is one of two (?)important criteria. A Blue card is .....";
                        }
                        else
                        {
                            message = @"FINAL EVAL: if a and no: You don't have the minimum salary required for a BlueCard, you are hence not eligible for a BlueCard. There is however the option for a work visa. A work visa is.......";
                        }
                    }
                    else if (work.DoYouHaveWorkContract.HasValue && work.DoYouHaveWorkContract == false)
                    {
                        message = @"Job hunting service? FINAL EVAL: As soon as you have a job offer that fulfills the minimum salary required for a BC and a recognized university degree you are eligible for a BC. There is also the option for a work visa. The BC is... the work visa is....";
                    }
                    //Anabin status-----------------------
                    if (work.UniversityInAnabin == true)
                    {
                        switch (work.AnabinStatus)
                        {
                            case "H+":
                                {
                                    message = @"You are eligible for Blue Card. Shall we help you with the process? ";
                                    break;
                                }
                            case "H+-":
                                {
                                    message = @"You might be eligible for Blue Card. Do you want us to do it?";
                                    break;
                                }
                            case "H-":
                                {
                                    message = @"You might be eligible for Blue Card. Do you want us to do it?";
                                    break;
                                }
                            default:
                                {
                                    message = @"You might be eligible for a work visa. Do you want us to do it?";
                                    break;
                                }
                        }
                    }
                }

            }
            else if (work.WorkSector_Id == 2 && work.DoYouHaveAcademicDegree.HasValue && work.DoYouHaveAcademicDegree == false)
            {
                message = @"qualified ? professional formatioequivalent to a German 3 years qualified formationn ?";
                if (work.ProfessionalQualification.HasValue && work.ProfessionalQualification == true)
                {
                    message = @"you might have chances, but your case has to be checked on an individual basis together with the German unemployment institution. For that you need to book our service here. Please contact us for an individual offer. Do you want us to send you an offer here?";
                }
                else if (work.ProfessionalQualification.HasValue && work.ProfessionalQualification == false)
                {
                    message = @"Sorry, your chances are quite low, we are not specialized in cases like yours  ";
                }
                else
                {
                    message = @"you might have chances, but your case has to be checked on an individual basis together with the German unemployment institution. For that you need to book our service here. Please contact us for an individual offer. Do you want us to send you an offer here?";
                }
            }
            //Engineer & Sciences--------------------------
            if ((work.WorkSector_Id == 4 || work.WorkSector_Id == 5) && work.DoYouHaveAcademicDegree.HasValue &&
                work.DoYouHaveAcademicDegree.GetValueOrDefault())
            {
                message = @"You have good chances to come to Germany, if you want to know more about your specific case please book our service here";
                if (work.DoYouHaveWorkContract.HasValue && work.DoYouHaveWorkContract == true)
                {
                    message = @"Is salary above 3,967 €/month (47,600 €/year)?";
                }
                else if (work.DoYouHaveWorkContract.HasValue && work.DoYouHaveWorkContract == false)
                {
                    message = @"Job hunting service? FINAL EVAL: As soon as you have a job offer that fulfills the minimum salary required for a BC and a recognized university degree you are eligible for a BC. There is also the option for a work visa. The BC is... the work visa is....";
                }
            }
            if ((work.WorkSector_Id == 4 || work.WorkSector_Id == 5) && work.DoYouHaveAcademicDegree.HasValue &&
                work.DoYouHaveAcademicDegree == false)
            {
                message = @"qualified ? professional formation ?";
                if (work.ProfessionalQualification.HasValue && work.ProfessionalQualification == true)
                {
                    message = @"you might have chances, please note that you haveto pay 50...";
                }
                else if (work.ProfessionalQualification.HasValue && work.ProfessionalQualification == false)
                {
                    message = @"Sorry, your chances are quite low, we are not specialized in cases like yours ";
                }
            }

            //hasUserPaid-----------------------------
            if (data.hasUserPaid.HasValue && data.hasUserPaid.GetValueOrDefault())
            {

            }
            return message;
        }

        private static string GetEntrepeneurMessage(Person data, Entrepeneur ent)
        {
            string message = @"You can come to Germany as an entrepreneur and start your own business here. Make sure that you present your business idea very detailed before you apply for a
freelance visa, i.e. business idea, business plan, assurance of financiation. Also the regional interest will play a role in whether your application is accepted as such. You won't be eligible for a visa if you do not have concrete plans yet. If so please go for the option of a job-seeking visa....
            if you want to have more information and if you want to know more about your specific case, you can book our service for more information (dann die Zahlungsmodalitäten-this service 
costs 50 euros , you can pay via credit card, pay pal etc)";

            if (data.hasUserPaid.HasValue && data.hasUserPaid.GetValueOrDefault())
            {
                
                if (ent.DoDocumentsExist.HasValue && ent.DoDocumentsExist.GetValueOrDefault())
                {
                    message = @"Do you already have the following documents:      
- Company Profile     
- Business Plan     
- Concept     
- Capital Requirements     
- Capital Budget     
- Business Forecast     
- Curriculum Vitae     
- Leaflet and Certificate of Health Insurance ?";
                    if (ent.DoDocumentsExistInGerman.HasValue && ent.DoDocumentsExistInGerman.GetValueOrDefault())
                    {
                        message = @"Is the financing of your enterprise secured through personal equity or by an approved credit?";
                        if (ent.DoYouHaveFinancing.HasValue && ent.DoYouHaveFinancing.GetValueOrDefault())
                        {
                            message = @"Please note: make sure that an economic interest or a regional need exists, which allows for the expectation that the activity will have positive consequences on the economy ";
                        }
                        else if (ent.DoYouHaveFinancing.HasValue && !ent.DoYouHaveFinancing.GetValueOrDefault())
                        {
                            message = @"Please make sure to provide a well elaborated business idea before you start applying for a visa in Germany and make sure to meet all the following requirements.

Please provide the following documents:
- Company Profile    
- Business Plan    
- Concept    
- Capital Requirements    
- Capital Budget    
- Business Forecast    
- Curriculum Vitae    
- Leaflet and Certificate of Health Insurance

Is the financing of your enterprise secured through personal equity or by an approved credit?

Please note: make sure that an economic interest or a regional need exists, which allows for the expectation that the activity will have positive consequences on the economy";
                        }
                    }
                    else if (ent.DoDocumentsExistInGerman.HasValue && !ent.DoDocumentsExistInGerman.GetValueOrDefault())
                    {
                        if (ent.DoYouRequireHelpInTranslation.HasValue)
                        {
                            message = @"Please contact us for the translation";
                            if (ent.DoYouHaveFinancing.HasValue && ent.DoYouHaveFinancing.GetValueOrDefault())
                            {
                                message = @"Please note: make sure that an economic interest or a regional need exists, which allows for the expectation that the activity will have positive consequences on the economy ";
                            }
                            else if (ent.DoYouHaveFinancing.HasValue && !ent.DoYouHaveFinancing.GetValueOrDefault())
                            {
                                message = @"Please make sure to provide a well elaborated business idea before you start applying for a visa in Germany and make sure to meet all the following requirements.

Please provide the following documents:
- Company Profile    
- Business Plan    
- Concept    
- Capital Requirements    
- Capital Budget    
- Business Forecast    
- Curriculum Vitae    
- Leaflet and Certificate of Health Insurance

Is the financing of your enterprise secured through personal equity or by an approved credit?

Please note: make sure that an economic interest or a regional need exists, which allows for the expectation that the activity will have positive consequences on the economy";
                            }
                        }
                    }
                }
                else if (ent.DoDocumentsExist.HasValue && !ent.DoDocumentsExist.GetValueOrDefault())
                {
                    message = @"Please make sure to provide a well elaborated business idea before you start applying for a visa in Germany and make sure to meet all the following requirements.
 Please provide the following documents:
 - Company Profile    
 - Business Plan    
 - Concept    
 - Capital Requirements    
 - Capital Budget    
 - Business Forecast    
 - Curriculum Vitae    
 - Leaflet and Certificate of Health Insurance
 Is the financing of your enterprise secured through personal equity or by an approved credit?
 Please note: make sure that an economic interest or a regional need exists, which allows for the expectation that the activity will have positive consequences on the economy
";
                }
                
            }


            return message;
        }

        private static string GetStudyMessage(Person data, Study study)
        {
            string message = string.Empty;
            if (study.DoYouDoApprenticeship.HasValue && study.DoYouDoApprenticeship.GetValueOrDefault())
            {
                message =
                    @"Great! Germany is looking for people willing to do a formation, especially in ertain fields like nursing, electricity... Here you have an overview of formaions available: LINK
    If you want to go please notethis service costs 50€";
            }
            if (study.FieldOfStudy_Id == 6)
            {
                message =
                    @"more research needed.you need to apply for a national visa (first 3 months valid, then after acceptance can be turned into student visa, no tourist visa! more info on http://www.hochschulkompass.de/en/degree-programmes/prerequisites-for-studying/citizenship.html#c4699 ";
            }
            return message;
        }

        private static string GetFamilyMessage(Person data, Family family)
        {
            string message = string.Empty;
            if (family != null && family.FamilyRelation_Id != null)
            {
                if (family.FamilyRelation_Id == 4 || family.FamilyRelation_Id == 5 || family.FamilyRelation_Id == 6)
                {
                    message = "RESEARCH";
                }
                if (family.FamilyRelation_Id == 1)
                {
                    message =
                        @"Please take into account that you only count as a spouse once you have legally recahed the official status. Take that into account for any visa application.";
                }
                if (family.FamilyRelation_Id == 3 && family.Spouse_Id == 5)
                {
                    message = @"You need to start a naturalisation procedure in order to apply for German citizenship.
Requirements:
. registration in Germany
. at least 16 years old
. legal status (some exceptions!)
. you are living in Germany for at least 3 years lawfully and habitually 
. married for at least 2 years . your German spouse is German citizen for al least 2 years
. you declare your belief in the principles of democratic constitutional order
. you make a living for yourself and your family from your own income (work contract)
. not subject of a criminal investigation
 . proof of at least B1 German knowledge . passed the citizenship test . willing to give up my current citizenship
Do you need help with the application process? Please contact us.";
                }
            }
            if (family.FamilyRelation_Id == 1 && family.Spouse_Id == 4)
            {
                message = @"You need to apply for a family reunion visa at the German mission in your home country. 
Requirements: 
. legally valid marriage certificate
. proof of the German ciritzenship of your family member living in Germany
. proof of your basic knowledge of the German language 

Do you need help with the application process? Please contact us
";
            }
            if (family.FamilyRelation_Id == 1 && family.Spouse_Id == 3)
            {
                message = @"You need to apply for a family reunion visa at the German mission in your home country. 
Requirements:  
. both marriage partners need to be older than 18 years old
. proof of your basic knowledge of the German language
. valid passports
. your spouse living in Germany needs to dispose of a legal status for at least 2 years
. proof that there are no reasons for expulsion
. sufficient living space available
. assured means of subsistence (proof of work contract etc.)

Do you need help with the application process? Please contact us
";
            }
            if (family.FamilyRelation_Id == 1 && family.Spouse_Id == 1)
            {
                message = @"In your visa application process you can directly indicate the moving to Germany together with your spouse + RESEARCH";
            }
            return message;
        }

        private static string GetMotivationMessage()
        {
            string message;
            message = @"These Institutions may help you
              . The Federal Office for Migration and Refugees. See here general information about the asylum procedure in Germany: http://www.bamf.de/SharedDocs/Anlagen/EN/Publikationen/Broschueren/das-deutsche-asylverfahren.pdf?__blob=publicationFile
                . Pro Asyl (association): http://www.proasyl.de/en/home/
                . http://www.amnesty-asylgruppe-berlin.de/Main/Asylberatung
                ";
            return message;
        }

        private static string GetTourismMessage(Person data, Tourism tour)
        {
            string message = string.Empty;
            message = @"You would like to visit Germany as a tourist? This is how you get a Tourist visa
    Only some citizens need to apply for a Tourist Visa in order to stay max. 90 days. Some other may enter Germany as a tourist without visa. 
Here is a list where you can check whether you need to apply for a Tourist Visa or not: http://www.auswaertiges-amt.de/EN/EinreiseUndAufenthalt/StaatenlisteVisumpflicht_node.html
        ";


            if (tour != null && tour.DoYouNeedToApply.HasValue)
            {
                message = tour.DoYouNeedToApply.GetValueOrDefault() ? @"Do you need help with the application process? Please contact us" :
                @"Please note that persons who do not require a visa to enter Germany (holders of passports from the countries marked “no” on the list) may not remain for more than 90 days in any six month period. Nor may they take up gainful employment whilst here.";
            }
            return message;
        }

        [HttpPost]
        public string EvaluateStatus(Person data, Work work, Family family, Tourism tour, Entrepeneur ent)
        {
            string message = string.Empty;

            if (data.Motivation_Id == 6)
            {
                message = @"These Institutions may help you
              . The Federal Office for Migration and Refugees. See here general information about the asylum procedure in Germany: http://www.bamf.de/SharedDocs/Anlagen/EN/Publikationen/Broschueren/das-deutsche-asylverfahren.pdf?__blob=publicationFile
                . Pro Asyl (association): http://www.proasyl.de/en/home/
                . http://www.amnesty-asylgruppe-berlin.de/Main/Asylberatung
                ";
            }

            return message;
        }

        [HttpGet]
        public string EvaluateStatus()
        {
            var data = new Person();
            string message = string.Empty;

            //if (data.Motivation_Id == 6)
            {
                message = @"These Institutions may help you
              . The Federal Office for Migration and Refugees. See here general information about the asylum procedure in Germany: http://www.bamf.de/SharedDocs/Anlagen/EN/Publikationen/Broschueren/das-deutsche-asylverfahren.pdf?__blob=publicationFile
                . Pro Asyl (association): http://www.proasyl.de/en/home/
                . http://www.amnesty-asylgruppe-berlin.de/Main/Asylberatung
                ";
            }

            return message;
        }

        [HttpGet]
        public ActionResult ShowMessage(string message)
        {
            ViewBag.Message = message;
            return View();
        }
    }
}
