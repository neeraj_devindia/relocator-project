﻿using System;
using System.Linq;
using System.Web.Routing;
using System.Web.Security;
using DAL;
using System.Web.Mvc;
using Relocator_Project.Models;
using WebMatrix.WebData;

namespace Relocator_Project.Controllers
{
    public class RegistrationController : Controller
    {
        private readonly IRegistrationOperation _registrationOperation;



        /// <summary>
        /// Only viewable for admin...
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        public ActionResult ListPeople()
        {
            return View(_registrationOperation.LoadPeople());
        }

        /// <summary>
        /// Load One Record for loged in user.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "User,Admin")]
        public ActionResult LoadPersonalDetails()
        {
            //return View(_registrationOperation.LoadPeople(User.Identity.Name));
            Person pers = _registrationOperation.LoadPersonal(User.Identity.Name.ToString());
            LoadViewBagsforDropDowns(pers);
            return View(pers);
        }

        [HttpGet]
        public ActionResult Create()
        {
            // ViewBag.Message = "Hello";
            LoadDropList();
            return View();
        }

        [HttpPost]
        [HandleError]
        public ActionResult Create(Person person, Work work, Entrepeneur entrepeneur, Study study, Family family, Tourism tourism, string hiddenId)
        {
            try
            {
                if (hiddenId == "Evaluate")
                {
                    return Evaluate(person, work, entrepeneur, study, family, tourism, hiddenId);
                }
                if (!ModelState.IsValid)
                {
                    LoadDropList();
                    return View(person);
                }
                else
                {
                    SetMotivation(person, work, entrepeneur, study, family, tourism);
                    SaveUser(person);
                    return RedirectToAction("Login", "UserPanel");
                }
            }
            catch (Exception e)
            {
                LoadDropList();
                ModelState.AddModelError("UserEx", e.Message);
                return View();
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details(int id)
        {
            Person pers = _registrationOperation.LoadPerson(id);
            LoadViewBagsforDropDowns(pers);
            return View(pers);
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            Person pers = _registrationOperation.LoadPerson(id);
            pers.Entrepeneur = null;
            ViewBag.Motivation_Id = new SelectList(_registrationOperation.GetMotivations(), "Id", "Name", pers.Motivation_Id);
            LoadViewBagsforDropDowns(pers);
            return View(pers);
        }
        [HttpPost]
        public ActionResult Edit(Person person, Work work, Entrepeneur entrepeneur, Study study, Family family, Tourism tourism)
        {
            person.Study = null;
            person.Work = null;
            person.Entrepeneur = null;
            person.Family = null;
            person.Tourism = null;
            {
                SetMotivation(person, work, entrepeneur, study, family, tourism);
            }
            _registrationOperation.EditPerson(person);
            return RedirectToAction("ListPeople");
        }

        public ActionResult Delete(string email)
        {
            DoDelete(email);
            return RedirectToAction("ListPeople");
        }

        #region Helper methods...


        /// <summary>
        /// Just extracted as helper method
        /// </summary>
        /// <param name="person"></param>
        private void SaveUser(Person person)
        {
            WebSecurity.CreateUserAndAccount(person.Email, person.Password);

            if (person.isAdmin == true)
            {
                Roles.AddUserToRole(person.Email, "Admin");
            }
            else
            {
                Roles.AddUserToRole(person.Email, "User");
            }
            _registrationOperation.SavePerson(person);
        }

        /// <summary>
        /// This method create view bags for Create View  that we use to populate dropdowns
        /// </summary>
        private void LoadDropList()
        {
            ViewBag.Motivation_Id = new SelectList(_registrationOperation.GetMotivations(), "Id", "Name");
            ViewBag.WorkSector_Id = new SelectList(_registrationOperation.LoaWorkSectors(), "Id", "Name");
            ViewBag.ITBranch_Id = new SelectList(_registrationOperation.LoadItBranches(), "Id", "Name");
            ViewBag.MedicalSector_id = new SelectList(_registrationOperation.LoadMedicalSectors(), "Id", "Name");
            ViewBag.FieldOfStudy_Id = new SelectList(_registrationOperation.LoadFieldsOfStudies(), "Id", "Name");
            ViewBag.UniversityType_Id = new SelectList(_registrationOperation.LoadUniversityTypes(), "Id", "Name");
            ViewBag.FamilyStatus_Id = new SelectList(_registrationOperation.LoadFamilyStatus(), "Id", "Name");
            ViewBag.LevelOfGerman_Id = new SelectList(_registrationOperation.LoadGerman(), "Id", "LevelName");
            ViewBag.StudyLevel_Id = new SelectList(_registrationOperation.LoadStudyLevel(), "Id", "Name");
            ViewBag.FamilyRelation_Id = new SelectList(_registrationOperation.LoadFamilyRelations(), "Id", "Name");
            ViewBag.Spouse_Id = new SelectList(_registrationOperation.LoadSpouses(), "Id", "Description");
        }

        private void LoadViewBagsforDropDowns(Person pers)
        {
            ViewBag.Motivation_Id = new SelectList(_registrationOperation.GetMotivations(), "Id", "Name", pers.Motivation_Id);
            if (pers.Work != null)
            {
                ViewBag.WorkSector_Id = new SelectList(_registrationOperation.LoaWorkSectors(), "Id", "Name",
                    pers.Work.WorkSector_Id);
                ViewBag.ITBranch_Id = new SelectList(_registrationOperation.LoadItBranches(), "Id", "Name",
                    pers.Work.ITBranch_Id);
                ViewBag.MedicalSector_id = new SelectList(_registrationOperation.LoadMedicalSectors(), "Id", "Name",
                    pers.Work.MedicalSector_Id);
            }
            if (pers.Study != null)
            {
                ViewBag.FieldOfStudy_Id = new SelectList(_registrationOperation.LoadFieldsOfStudies(), "Id", "Name",
                    pers.Study.FieldOfStudy_Id);
                ViewBag.UniversityType_Id = new SelectList(_registrationOperation.LoadUniversityTypes(), "Id", "Name",
                    pers.Study.UniversityType_Id);
            }
            ViewBag.FamilyStatus_Id = new SelectList(_registrationOperation.LoadFamilyStatus(), "Id", "Name",
                pers.FamilyStatus_Id);
            ViewBag.LevelOfGerman_Id = new SelectList(_registrationOperation.LoadGerman(), "Id", "LevelName",
                pers.LevelOfGerman_Id);
            ViewBag.StudyLevel_Id = new SelectList(_registrationOperation.LoadStudyLevel(), "Id", "Name", pers.StudyLevel_Id);
            if (pers.Family != null)
            {
                ViewBag.FamilyRelation_Id = new SelectList(_registrationOperation.LoadFamilyRelations(), "Id", "Name",
                    pers.Family.FamilyRelation_Id);
                ViewBag.Spouse_Id = new SelectList(_registrationOperation.LoadSpouses(), "Id", "Description",
                    pers.Family.Spouse_Id);
            }
        }

        /// <summary>
        /// This is just nonsense... :-) TO Show evaluating.. will be different..
        /// </summary>
        /// <param name="person"></param>
        /// <param name="work"></param>
        /// <param name="entrepeneur"></param>
        /// <param name="study"></param>
        /// <param name="family"></param>
        /// <param name="tourism"></param>
        /// <param name="hiddenId"></param>
        /// <returns></returns>
        private ActionResult Evaluate(Person person, Work work, Entrepeneur entrepeneur, Study study, Family family,
                                      Tourism tourism, string hiddenId)
        {
            // Reroute (?) maybe etc.
            ViewBag.Message = UserMessageController.EvaluateStatus(person, work, family, tourism, study, entrepeneur,
                                                                   hiddenId);
            LoadDropList();
            return View("Create");
            //return RedirectToAction("ShowMessage", "UserMessage", new RouteValueDictionary(
            //                                                          new
            //                                                              {
            //                                                                  controller = "UserMessage",
            //                                                                  action = "ShowMessage",
            //                                                                  message =
            //                                                              UserMessageController.EvaluateStatus(person, work,
            //                                                                                                   family, tourism,
            //                                                                                                   study,
            //                                                                                                   entrepeneur,
            //                                                                                                   hiddenId)
            //                                                              }));
        }



        /// <summary>
        /// Just extracted as helper method
        /// </summary>
        /// <param name="person"></param>
        /// <param name="work"></param>
        /// <param name="entrepeneur"></param>
        /// <param name="study"></param>
        /// <param name="family"></param>
        /// <param name="tourism"></param>
        private static void SetMotivation(Person person, Work work, Entrepeneur entrepeneur, Study study, Family family,
                                          Tourism tourism)
        {
            switch (person.Motivation_Id)
            {
                case 1:
                    {
                        person.Work = work;
                        break;
                    }
                case 2:
                    {
                        person.Entrepeneur = entrepeneur;
                        break;
                    }
                case 3:
                    {
                        person.Study = study;
                        break;
                    }
                case 4:
                    {
                        person.Family = family;
                        break;
                    }
                case 5:
                    {
                        person.Tourism = tourism;
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }


        /// <summary>
        /// Just extracted as helper method.
        /// </summary>
        /// <param name="email"></param>
        private void DoDelete(string email)
        {
            _registrationOperation.DeletePerson(email);
            var rolesProvider = (SimpleRoleProvider)Roles.Provider;
            var roles = rolesProvider.GetRolesForUser(email);
            if (roles != null && roles.Length > 0)
            {
                rolesProvider.RemoveUsersFromRoles(new[] { email }, roles);
            }
            Membership.DeleteUser(email, true);
            ((SimpleMembershipProvider)Membership.Provider).DeleteAccount(email);
            // deletes record from webpages_Membership table
            ((SimpleMembershipProvider)Membership.Provider).DeleteUser(email, true);
            ((SimpleMembershipProvider)Membership.Provider).DeleteAccount(email);
        }

        #endregion

        #region json


        /// <summary>
        /// Method to Get the Json Data
        /// </summary>
        /// <param name="obj">Objects</param>
        /// <returns>Json Data</returns>
        private JsonResult GetData(object obj)
        {
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        public RegistrationController()
        {
            _registrationOperation = new RegistrationOperation();
        }
        public JsonResult GetAllMotivations()
        {
            return GetData(_registrationOperation.GetMotivations());
        }
        public JsonResult LoadSlectedMotivationTable(int id)
        {
            return GetData(_registrationOperation.GetMotivationSelectedTableList(id));
        }
        public JsonResult LoadWorkSectors()
        {
            return GetData(_registrationOperation.LoaWorkSectors());
        }
        public JsonResult LoadItBranches()
        {
            return GetData(_registrationOperation.LoadItBranches());
        }
        public JsonResult LoadMedicalSectors()
        {
            return GetData(_registrationOperation.LoadMedicalSectors());
        }

        #endregion
    }
}
