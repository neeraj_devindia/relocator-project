﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DAL;
using WebMatrix.WebData;

namespace Relocator_Project.Controllers
{
    [AllowAnonymous]
    public class UserPanelController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Person data)
        {
            bool success = WebSecurity.Login(data.Email, data.Password, false);
            if (success)
            {
                FormsAuthentication.RedirectFromLoginPage(data.Email, false);
            }
            return View();
        }

        public ActionResult Logout()
        {
            WebSecurity.Logout();
            return RedirectToAction("Login", "UserPanel");
        }
      
    }
}
