﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Relocator_Project.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize(Roles = "Admin,User")]
    public class LoginSuccessController : Controller
    {
        public ActionResult Index()
        {
            if (User.IsInRole("Admin"))
            {
                return RedirectToRoute(new { action = "AdminHome", controller = "LoginSuccess" });
            }
            else
            {
                return RedirectToRoute(new { action = "UserHome", controller = "LoginSuccess" });
            }
        }
        [Authorize(Roles = "Admin")]
        public ActionResult AdminHome()
        {
            return View();
        }

        public ActionResult UserHome()
        {
            return View();
        }
    }
}
