﻿
function HideCtrls(idArray) {
    $.each(idArray, function (index, item)
    {
        $("#" + item).css("display", "none");
    });
}

function ShowCtrls(idArray) {
    $.each(idArray, function (index, item) {
        $("#" + item).css("display", "inline");
    });
}

