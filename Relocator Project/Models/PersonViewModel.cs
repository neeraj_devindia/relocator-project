﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL;

namespace Relocator_Project.Models
{
    public class PersonViewModel
    {
        public Person Person { get; set; }
        public Work Work { get; set; }
    }
}