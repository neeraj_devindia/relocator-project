﻿
$(function () {

    $('#Motivation_Id').on('change', function (e) {

        var valueSelected = this.value;
        dosome(valueSelected);
    }
    );
});


function dosome(id) {

    $("#div_Work").css("display", "none");

    $("#div_Entrepeneur").css("display", "none");

    $("#div_LearnStudy").css("display", "none");

    $("#div_Family").css("display", "none");

    $("#div_Tourism").css("display", "none");

    $("#div_Asylum").css("display", "none");

    switch (id) {
        case "1":
            {
                $("#div_Work").css("display", "inline");

                HideCtrls(["div_ITBranch_Id", "div_MedicalSector_Id", "div_DoYouHaveAcademicDegree", "div_ProfessionalQualification", "div_ProfessionalQualification", "div_DoYouHaveWorkContract", "div_SalaryAmount", "div_University", "div_UniversityInAnabin", "div_AnabinStatus"]);
                $('#WorkSector_Id').val("");
                $('#ITBranch_Id').val(""); //Set select index=0
                $('#MedicalSector_Id').val("");
                $('#DoYouHaveAcademicDegree').val("");
                $('#DoYouHaveWorkContract').val("");
                $('#ProfessionalQualification').val("");
                $('#UniversityInAnabin').val("");
                break;
            }
        case "2":
            {
                $("#div_Entrepeneur").css("display", "inline");
                HideCtrls(["div_DoDocumentsExistInGerman", "div_DoYouRequireHelpInTranslation", "div_DoYouNeedAdvice", "div_DoYouHaveFinancing"]);

                $('#DoDocumentsExistInGerman').val("");
                $('#DoYouRequireHelpInTranslation').val("");
                $('#DoYouNeedAdvice').val("");
                $('#DoYouHaveFinancing').val("");
                break;
            }
        case "3":
            {
                $("#div_LearnStudy").css("display", "inline");
                HideCtrls(["div_FieldOfStudy_Id", "div_StudyLevel_Id", "div_UniversityType_Id", "div_DoYouDoLanguageCourse", "div_DoYouDoExchangeProgram", "div_DoYouDoApprenticeship"]);
                $('#DoYouHaveEnoughMoney').val("");
                $('#FieldOfStudy_Id').val("");
                $('#StudyLevel_Id').val("");
                $('#UniversityType_Id').val("");
                $('#DoYouDoLanguageCourse').val("");
                $('#DoYouDoExchangeProgram').val("");
                $('#DoYouDoApprenticeship').val("");
                break;
            }
        case "4":
            {
                $("#div_Family").css("display", "inline");

                HideCtrls(["div_Spouse_Id"]);
                $('#FamilyRelation_Id').val("");
                $('#Spouse_Id').val("");
                break;
            }
        case "5":
            {
                $("#div_Tourism").css("display", "inline");
                $('#DoYouNeedToApply').val("");
                break;
            }
        case "6":
            {
                  $("#div_Asylum").css("display", "inline");
                break;
            }
        default:
            break;
    }

}

